USE [BookStoreDb];

INSERT INTO [Stores] ([Name], [Adres]) VALUES
('Tania', 'Mostowa 32'),
('Droga', 'Kacza 43'),
('Dobra', 'Wrzosowa 34');

INSERT INTO [Authors] ([Name], [Surname], [BirthYear]) VALUES
('Jan', 'Kowalski', 1980),
('Tomasz', 'Nowak', 1990),
('Zbigniew', 'Bak', 1974),
('Anna', 'Zientarska', 1992),
('Monika', 'Polak', 1990),
('John', 'Smith', 1993);

INSERT INTO [Books]([Title], [AuthorId], [StoreId], [Price], [PublishYear], [Sold]) VALUES
('New Spring', 1, 1, 22.33, 2000, 0),	
('The Eye of the World', 3, 1, 22.33, 2004, 0),
('The Great Hunt', 4, 2, 43.33, 2002, 2),
('The Dragon Reborn', 1, 3, 76.33, 2003, 3),
('The Shadow Rising', 5, 1, 36.33, 2001, 5),
('The Fires of Heaven', 2, 3, 44.33, 1999, 4),
('Lord of Chaos', 1, 2, 15.33, 2005, 0),
('A Crown of Swords', 4, 3, 12.66, 2008, 6),
('The Path of Daggers', 5, 2, 35.45, 2012, 7),
('Winters Heart', 6, 3, 22.44, 2002, 0),
('Crossroads of Twilight', 3, 1, 33.12, 2003, 4),
('Knife of Dreams', 3, 2, 55.02, 2004, 10),
('The Gathering Storm', 2, 3, 44.20, 2002, 12),
('Towers of Midnight', 6, 1, 12.33, 2003, 5),
('A Memory of Light', 6, 2, 19.60, 2003, 4),
('Wheel of Time', 4, 3, 22.00, 2005, 0);