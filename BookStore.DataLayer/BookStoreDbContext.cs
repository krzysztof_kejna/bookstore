﻿using BookStore.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataLayer
{
    
    public class BookStoreDbContext: DbContext
    {
        private const string _connectionString = "Data Source = .;Initial Catalog = BookStoreDb;Integrated Security=True;";

        public DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public DbSet<Store> Stores { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }
    }
}
