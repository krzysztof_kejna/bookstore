﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataLayer.Models
{
    public class Author:Entity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int BirthYear { get; set; }
    }
}
