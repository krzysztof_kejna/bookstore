﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataLayer.Models
{
    public class Store:Entity
    {
        public string Name { get; set; }
        public string Adres { get; set; }
    }
}
