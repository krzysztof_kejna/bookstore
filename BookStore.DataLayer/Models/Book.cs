﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataLayer.Models
{
    public class Book:Entity
    {
        public string Title { get; set; }
        public int PublishYear { get; set; }
        public int AuthorId { get; set; }
        public Author Author { get; set; }
        public int StoreId { get; set; }
        public Store Store { get; set; }
        public double Price { get; set; }
        public int Sold { get; set; }
    }
}
