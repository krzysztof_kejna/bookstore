﻿using BookStore.DataLayer;
using BookStore.DataLayer.Models;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BookStore.BusinessLayer.Tests
{
    public class BookServiceTests
    {
        [Test]
        public void ThreeBooksInList_ReturnsListWithThreeElements()
        {
            var books = new List<Book>
            {
                new Book {StoreId = 2, Title = "bookOne" },
                new Book {StoreId = 2, Title = "bookTwo", Sold = 2 },
                new Book {StoreId = 2, Title = "bookThree" },
            };
            Mock<IRaportGeneratorService> getAllBooksMock = new Mock<IRaportGeneratorService>();
            getAllBooksMock.Setup(book => book.GetAll(It.IsAny<int>())).Returns(books);
            var service = new BookService(getAllBooksMock.Object);
            service.GetSortBySold(2).Count.Should().Be(3);            
        }

        [Test]
        public void BookTwoIsSoldTwoTimes_ReturnsListWithFirstElementBookTwo()
        {
            var books = new List<Book>
            {
                new Book {StoreId = 2, Title = "bookOne" },
                new Book {StoreId = 2, Title = "bookTwo", Sold = 2 },
                new Book {StoreId = 2, Title = "bookThree" },
            };
            Mock<IRaportGeneratorService> getAllBooksMock = new Mock<IRaportGeneratorService>();
            getAllBooksMock.Setup(book => book.GetAll(It.IsAny<int>())).Returns(books);
            var service = new BookService(getAllBooksMock.Object);
            service.GetSortBySold(2)[0].Should().Be(books[1]);
        }

        [Test]
        public void BookTwoIsSoldTwoTimesAndBookThreeIsSold5Times_ReturnsListWithSecondElementBookTwo()
        {
            var books = new List<Book>
            {
                new Book {StoreId = 2, Title = "bookOne" },
                new Book {StoreId = 2, Title = "bookTwo", Sold = 2 },
                new Book {StoreId = 2, Title = "bookThree", Sold = 5 },
            };
            Mock<IRaportGeneratorService> getAllBooksMock = new Mock<IRaportGeneratorService>();
            getAllBooksMock.Setup(book => book.GetAll(It.IsAny<int>())).Returns(books);
            var service = new BookService(getAllBooksMock.Object);
            service.GetSortBySold(2)[1].Should().Be(books[1]);
        }

        [Test]
        public void MostSoldBookIsSold50_ReturnsFirstElementWith50Sold()
        {
            var books = new List<Book>
            {
            new Book {StoreId = 2, Title = "bookOne", Sold = 30 },
            new Book {StoreId = 2, Title = "bookTwo", Sold = 15 },
            new Book {StoreId = 2, Title = "bookThree", Sold = 50 },
            };

            Mock<IRaportGeneratorService> getAllBooksMock = new Mock<IRaportGeneratorService>();
            getAllBooksMock.Setup(book => book.GetAll(It.IsAny<int>())).Returns(books);
            var service = new BookService(getAllBooksMock.Object);
            service.GetSortBySold(2)[0].Sold.Should().Be(50);
        }

        [Test]

        public void OutputIsSameAsTestFile()
        {
            JsonConversionService json = new JsonConversionService();
            var authors = new List<Author>
            {
                new Author { Id = 1, Name = "Jan", Surname = "Kowalski", BirthYear = 1992},
                new Author { Id = 2, Name = "Zygmunt", Surname = "Nowak", BirthYear = 2001},
                new Author { Id = 3, Name = "Bon", Surname = "Jovi", BirthYear = 1983},
            };

            var books = new List<Book>
            {
            new Book {AuthorId = 1, Author = authors[0], StoreId = 1, Price = 33.44, PublishYear = 2011, Sold = 3, Id = 3, Title = "Wiosna"},
            new Book {AuthorId = 2, Author = authors[1], StoreId = 1, Price = 1993, PublishYear = 2019, Sold = 3, Id = 4, Title = "Lato"},
            new Book {AuthorId = 1, Author = authors[0], StoreId = 1, Price = 22.33, PublishYear = 2012, Sold = 2, Id = 1, Title = "Zima"},
            new Book {AuthorId = 3, Author = authors[2], StoreId = 1, Price = 2014, PublishYear = 2011, Sold = 0, Id = 2, Title = "Jesien"},
            };

            Mock<IRaportGeneratorService> getAllBooksMock = new Mock<IRaportGeneratorService>();
            getAllBooksMock.Setup(book => book.GetAll(It.IsAny<int>())).Returns(books);
            var service = new BookService(getAllBooksMock.Object);
            json.Conversion("Testing.Json", service.GetSortBySold(1));

            var testOrigin = File.ReadAllBytes(@"C:\Users\student\source\repos\BookStore\BookStore.BusinessLayer.Tests\Test.Json");
            var testFile = File.ReadAllBytes("Testing.Json");
            if (testOrigin.Length != testFile.Length)
            {
                Assert.Fail("Output is different than Origin!");
            }

            for (int i = 0; i < testOrigin.Length; i++)
            {
                if (testOrigin[i] != testFile[i])
                {
                    Assert.Fail("Output is different than Origin!");
                }
            }
        }
    }
}
