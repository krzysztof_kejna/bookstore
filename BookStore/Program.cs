﻿using BookStore.BusinessLayer;
using BookStore.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace BookStore.Cli
{
    public class Program
    {
        private int _choosenStoreId;
        private AuthorService _authorService;
        private BookService _bookService;
        private StoreService _storeService;
        private EarningService _earningService;
        private RaportGeneratorService _raportGeneratorService;
        private DatabaseInitializer _databaseInitializer;
        private JsonConversionService _jsonConversion;
        private IoHelper _ioHelper;
        private Menu _menu;


        public Program()
        {   
            _authorService =            new AuthorService();
            _storeService =             new StoreService();
            _earningService =           new EarningService();
            _raportGeneratorService =   new RaportGeneratorService();
            _bookService =              new BookService(new RaportGeneratorService());
            _databaseInitializer =      new DatabaseInitializer();
            _jsonConversion =           new JsonConversionService();
            _ioHelper =                 new IoHelper();
            _menu =                     new Menu();
        }

        static void Main(string[] args)
        {           
           new Program().Start();
        }

        private void Start()
        {
            InitializeDatabase();

            while (_choosenStoreId == 0)
            {
                ChooseStore();
            }

            InitializeMenu();



            while(true)
            {
                _menu.ShowMenu();

                int input = _ioHelper.GetIntFromUser("Enter command number");

                Console.Clear();

                try
                {
                    _menu.ExecuteAction(input);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Command execution failed: {e.Message}");
                }
            }
        }

        private void InitializeDatabase()
        {
            _databaseInitializer.Initialize();
        }

        private void InitializeMenu()
        {
            _menu.AddMenuItem(AddingAuthor,     "Add Author",                           1);
            _menu.AddMenuItem(AddingBook,       "Add Book",                             2);
            _menu.AddMenuItem(BookList,         "Print all book",                       3);
            _menu.AddMenuItem(AuthorList,       "Print all books by author surname",    4);
            _menu.AddMenuItem(SellBook,         "Sell book",                            5);
            _menu.AddMenuItem(SoldBooks,        "Print sold books",                     6);
            _menu.AddMenuItem(PrintAllBySold,   "Print all books by sold",              7);
            _menu.AddMenuItem(ChooseStore,      "Add or choose Store",                  8);
            _menu.AddMenuItem(Exit,             "Exit",                                 9);
        }

        private void AddingAuthor()
        {
            Console.WriteLine("Chosen option: 1. Add Author\n");
            Author newAuthor = new Author
            {
                Name = _ioHelper.GetStringFromUser("Name"),
                Surname = _ioHelper.GetStringFromUser("Surname"),
                BirthYear = _ioHelper.GetIntFromUser("Birth year")
            };
            _authorService.AddAuthor(newAuthor);
            _ioHelper.PrintAndClear($"Author added with Id: {newAuthor.Id}, press any key to return to menu");
        }

        private void AddingBook()
        {
            Console.WriteLine("Chosen option : 2. Add Book\n");
            Book newBook = new Book();
            newBook.Title = _ioHelper.GetStringFromUser("Title");
            newBook.PublishYear = _ioHelper.GetIntFromUser("Publish year");
            newBook.Sold = 0;
            var bookAuthorId = _ioHelper.GetIntFromUser("Author Id");
            var price = _ioHelper.GetDoubleFromUser("Price");
            var check = price;

            PriceFormatCheck(ref price, ref check);

            newBook.Price = price;
            _bookService.AddBook(_choosenStoreId, bookAuthorId, newBook);
            _ioHelper.PrintAndClear($"Book added, press any key to return to menu");
        }

        private void PriceFormatCheck(ref double price, ref double check)
        {
            while (check * 100 % 1 >= double.Epsilon)
            {
                Console.WriteLine("Wrong price, please try again");
                price = _ioHelper.GetDoubleFromUser("Price");
                check = price * 100 % 1;
            }
        }

        private void BookList()
        {
            Console.WriteLine("Chosen option : 3. Print all books\n");
            var allBooks = _raportGeneratorService.GetAll(_choosenStoreId);
            _ioHelper.PrintBooksInfo(allBooks);
            _ioHelper.PrintAndClear("Press any key to return to menu");
        }

        private void AuthorList()
        {
            Console.WriteLine("Chosen option : 4. Print all books by author surname\n");
            string author = _ioHelper.GetStringFromUser("author surname:");
            Console.Clear();
            var allAuthorBooks = _bookService.GetAllByAuthorSurname(_choosenStoreId, author);
            _ioHelper.PrintBooksInfo(allAuthorBooks);
            _ioHelper.PrintAndClear("Press any key to return to menu");
        }

        private void SellBook()
        {
            Console.WriteLine("Chosen option : 5. Sell book\n");
            var id = _ioHelper.GetIntFromUser("Book Id");
            var book = _bookService.GetBookByID(_choosenStoreId, id);
            _bookService.SellBook(_choosenStoreId, book);
            _ioHelper.PrintAndClear("Book sold, press any key to return to menu");
        }

        private void SoldBooks()
        {
            Console.WriteLine("Chosen option : 6. Print sold books\n");

            foreach(Book book in _bookService.GetAllSold(_choosenStoreId))
            {
                _ioHelper.PrintSoldBooksInfo(book);
            }
            
            if(_bookService.GetAllSold(_choosenStoreId).Count == 0)
            {
                Console.WriteLine("No item to show");
            }
            _ioHelper.PrintAndClear("\nPress any key to return to menu");
        }

        private void PrintAllBySold()
        {
            Console.WriteLine("Chosen option : 7. Print all books by sold\n");
            var lineWriten = false;
  
            foreach (var book in _bookService.GetSortBySold(_choosenStoreId))
            {
                if (book.Sold == 0 && lineWriten == false)
                { 
                    lineWriten = true;
                    Console.WriteLine("----------------------------------------------------------------------------------------");
                }
                _ioHelper.PrintSoldBooksInfo(book);
            }

            if (_bookService.GetSortBySold(_choosenStoreId).Count == 0)
            {
                Console.WriteLine("No item to show");
                _ioHelper.PrintAndClear("\nPress any key to return to menu");
            }
            else
            {
                var printJson = _bookService.GetSortBySold(_choosenStoreId).Count != 0 && (_ioHelper.GetStringFromUser("PRINT to print report to json file type") == "PRINT");
                if (printJson)
                {
                    var path = _ioHelper.GetStringFromUser("type path to file:");
                    _jsonConversion.Conversion(path, _bookService.GetSortBySold(_choosenStoreId));
                }

                _ioHelper.PrintAndClear("\nPress any key to return to menu");
            }
        }

        private int AddStore()
        {
            var newStore = new Store()
            {
                Name = _ioHelper.GetStringFromUser("Store name:"),
                Adres = _ioHelper.GetStringFromUser("Adres")
            };

            return _storeService.AddStore(newStore);
        }

        private void ChooseStore()
        {
            var numberOfStores = _storeService.CountOfStores();
            if (numberOfStores == 0)
            {
                Console.WriteLine("Store list is empty please add new store");
                _choosenStoreId = AddStore();
                Console.Clear();
            }
            else
            {
                Console.WriteLine("Choose store from list or add new");
                PrintListOfStores(numberOfStores);

                int input = _ioHelper.GetIntFromUser("Enter command number");
                if (input > 0 && input <= numberOfStores)
                {
                    _choosenStoreId = input;
                    Console.Clear();
                }
                else if (input == _storeService.CountOfStores() + 1)
                {
                    _choosenStoreId = AddStore();
                    Console.Clear();
                }
                else
                {
                    _choosenStoreId = 0;
                    Console.WriteLine("Wrong command number, please try again");
                }
            }
        }

        private void PrintListOfStores(int storesNumber)
        {
            foreach (var store in _storeService.GetAllStores())
            {
                Console.WriteLine($"{store.Id} {store.Name} {store.Adres}");
            }

            Console.WriteLine($"\n{storesNumber + 1} Add new store");
        }

        private void Exit()
        {
            Environment.Exit(0);
        }
    }
}
