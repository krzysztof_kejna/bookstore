﻿using BookStore.BusinessLayer;
using BookStore.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace BookStore.Cli
{
    internal class IoHelper
    {
        EarningService _earningService = new EarningService();

        public string GetStringFromUser(string message)
        {
            Console.WriteLine($"Type {message}");
            return Console.ReadLine();
        }

        public int GetIntFromUser(string message)
        {
            Console.WriteLine($"Type {message}");

            int result;
            while (!int.TryParse(Console.ReadLine(), out result))
            {
                Console.WriteLine("Wrong input, please type number");
            }

            return result;
        }

        public double GetDoubleFromUser(string message)
        {
            Console.WriteLine($"Type {message}");
            double result;
            string replace = Console.ReadLine();
            replace = replace.Replace(",", ".");
            while (!double.TryParse(replace, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                Console.WriteLine("Wrong input, please type number");
                replace = Console.ReadLine();
                replace = replace.Replace(",", ".");
            }
            return result;
        }

        public void PrintAndClear(string message)
        {
            Console.WriteLine(message);
            Console.ReadLine();
            Console.Clear();
        }

        public void PrintBooksInfo(List<Book> bookList)
        {
            foreach (Book book in bookList)
            {
                Console.WriteLine($"#{bookList.IndexOf(book) + 1}");
                Console.WriteLine($"Book Id:{book.Id}");
                Console.WriteLine($"Author name:{book.Author.Name}");
                Console.WriteLine($"Author surname:{book.Author.Surname}");
                Console.WriteLine($"Book title:{book.Title}");
                Console.WriteLine($"Book publish year:{book.PublishYear}");
                Console.WriteLine($"Book price:{book.Price.ToString("F", CultureInfo.InvariantCulture)}\n");
            }

            if (bookList.Count == 0)
            {
                Console.WriteLine("No item to show");
            }
        }

        public void PrintSoldBooksInfo(Book book)
        {
            var earnings = _earningService.EarningCalculation(book);
            Console.WriteLine($"Title: {book.Title} Author: {book.Author.Name} {book.Author.Surname} Copies Sold: {book.Sold} Earnings: {earnings.ToString("F2", CultureInfo.InvariantCulture)}");
        }

    }
}
