﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Cli
{
    internal class MenuItem
    {
        public string Description;
        public Action Action;
    }
}
