﻿using System;
using System.Collections.Generic;

namespace BookStore.Cli
{
    internal class Menu
    {
        private Dictionary<int, MenuItem> _menuItems = new Dictionary<int, MenuItem>();

        public void AddMenuItem(Action action, string descritpion, int key)
        {
            if(_menuItems.ContainsKey(key))
            {
                throw new Exception($"There is already a menu item with key {key}");
            }

            _menuItems.Add(
                key,
                new MenuItem {
                    Action = action,
                    Description = descritpion
                });
        }

        public void ExecuteAction(int key)
        {
            if (!_menuItems.ContainsKey(key))
            {
                throw new Exception($"There is no action for key {key} defined");
            }

            _menuItems[key].Action();
        }

        public void ShowMenu()
        {
            Console.WriteLine("Avalible options:");
            foreach(var item in _menuItems)
            {
                Console.WriteLine($"{item.Key} {item.Value.Description}");
            }
        }
    }
}
