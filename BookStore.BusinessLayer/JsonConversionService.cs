﻿using BookStore.DataLayer.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BookStore.BusinessLayer
{
    public class JsonConversionService
    {
        public void Conversion(string path, List<Book> bookList)
        {
            string json = JsonConvert.SerializeObject(bookList);
            File.WriteAllText(path, json); 
        }

        public object DeConversion(string path)
        {
            return JsonConvert.DeserializeObject(path);
        }
    }
}
