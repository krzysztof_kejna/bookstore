﻿using BookStore.DataLayer;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLayer
{
    public class DatabaseInitializer
    {
        public void Initialize()
        {
            using (var ctx = new BookStoreDbContext())
            {
                //ctx.Database.EnsureDeleted();

                ctx.GetInfrastructure().GetService<IMigrator>().Migrate();
            }
        }
    }
}
