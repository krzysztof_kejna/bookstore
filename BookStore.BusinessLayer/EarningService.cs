﻿
using BookStore.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLayer
{
    public class EarningService
    {       
        public double EarningCalculation(Book book)
        {
            return book.Price * book.Sold;
        }
    }
}
