﻿using BookStore.DataLayer;
using BookStore.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookStore.BusinessLayer
{
    public class StoreService
    {   
        public List<Store> GetAllStores()
        {
            using (var context = new BookStoreDbContext())
            {
                return context.Stores.ToList();
            }
        }

        public int CountOfStores()
        {
            using (var context = new BookStoreDbContext())
            {
                return context.Stores.Count();
            }
        }

        public int AddStore(Store store)
        {
            int id;

            using (var context = new BookStoreDbContext())
            {
                var createdStoreEntry = context.Stores.Add(store);

                context.SaveChanges();

                id = createdStoreEntry.Entity.Id;
            }

            return id;
        }
    }
}
