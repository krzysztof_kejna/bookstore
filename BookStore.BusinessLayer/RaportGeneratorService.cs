﻿using BookStore.DataLayer;
using BookStore.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookStore.BusinessLayer
{
    public class RaportGeneratorService : IRaportGeneratorService
    {
        public List<Book> GetAll(int storeId)
        {
            using (var context = new BookStoreDbContext())
            {
                return context.Books
                    .Include(b => b.Author)
                    .Where(b => b.StoreId == storeId)
                    .ToList();
            }
        }
    }
}
