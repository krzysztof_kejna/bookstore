﻿using BookStore.DataLayer;
using BookStore.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookStore.BusinessLayer
{
    public class AuthorService
    {
        public int AddAuthor(Author author)
        {
            int id;

            using (var context = new BookStoreDbContext())
            {
                var createdAuthorEntry = context.Authors.Add(author);

                context.SaveChanges();

                id = createdAuthorEntry.Entity.Id;
            }

            return id;
        }

        public List<Author> GetAll()
        {
            using (var context = new BookStoreDbContext())
            {
                return context.Authors.ToList();
            }
        }

        public Author GetAuthorById(int id)
        {
            using (var context = new BookStoreDbContext())
            {
                return context.Authors.FirstOrDefault(a => a.Id == id);
            }
        }      
    }
}
