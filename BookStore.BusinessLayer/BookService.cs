﻿
using BookStore.DataLayer;
using BookStore.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookStore.BusinessLayer
{
    public class BookService
    {
        AuthorService _authorService = new AuthorService();
        StoreService _storeService = new StoreService();
        private IRaportGeneratorService _raportGeneratorService;

        public BookService(IRaportGeneratorService raportGeneratorService)
        {
            _raportGeneratorService = raportGeneratorService; 
        }

        public int AddBook(int storeId, int authorId, Book book)
        {
            var choosenAuthor = _authorService.GetAuthorById(authorId);

            if (choosenAuthor == null)
            {
                throw new Exception($"There is no author with ID{authorId}");
            }

            book.StoreId = storeId;
            book.AuthorId = authorId;

            using (var context = new BookStoreDbContext())
            {
                var bookEntity = context.Add(book);
                context.SaveChanges();
                return bookEntity.Entity.Id;
            }
        }

        public List<Book> GetAllByAuthorSurname(int storeId, string surname)
        {
            using (var context = new BookStoreDbContext())
            {
                return context.Books
                    .Include(b => b.Author)
                    .Where(b => b.Author.Surname == surname)
                    .Where(b => b.StoreId == storeId)
                    .ToList();
            }
        }

        public List<Book> GetAllSold(int storeId)
        {
            using (var context = new BookStoreDbContext())
            {
                return context.Books
                    .Where(b => b.Sold > 0)
                    .Where(b => b.StoreId == storeId)
                    .Include(b => b.Author)
                    .ToList();
            }
        }

        public List<Book> GetSortBySold(int storeId)
        {
            return _raportGeneratorService.GetAll(storeId).OrderByDescending(b => b.Sold).ToList();                 
        }

        public void SellBook(int storeId, Book book)
        {
            if(book == null || book.StoreId != storeId)
            {
                throw new Exception("There is no book with that id");
            }

            book.Sold += 1;
            using (var context = new BookStoreDbContext())
            {
                context.Update(book);
                context.SaveChanges();
            }
        }

        public Book GetBookByID(int storeId, int id)
        {
            using (var context = new BookStoreDbContext())
            {
                return context.Books
                    .Include(b => b.Author)
                    .Where(b => b.StoreId == storeId)
                    .FirstOrDefault(b => b.Id == id);
            }
        }
    }
}
