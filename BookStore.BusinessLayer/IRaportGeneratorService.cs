﻿using BookStore.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLayer
{
    public interface IRaportGeneratorService
    {
        List<Book> GetAll(int storeId);
    }
}
